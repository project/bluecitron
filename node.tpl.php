<?php
?>
<?php phptemplate_comment_wrapper(NULL, $node->type); ?>

<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
 <?php if (!$teaser) { ?>
    <?php if ($links): ?>
      <div class="links">
      <div class="clearer"></div>
			<?php print $links; ?>
      <div class="clearer"></div>
      </div>
    <?php endif; ?>
    <?php } ?>
    
<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2 class="withlink"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  

  <div class="content">
    <?php print $content ?>
  </div>

  <div class="clear-block clear">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms">
      <img src="<?php global $base_url; print $base_url. '/' .path_to_theme() ?>/images/tag.png" alt="tags" class="termicon"/>
        <div class="termright"><?php print $terms ?></div>
      
      <div class="clearer"></div>
      </div>
    <?php endif;?>
    </div>

  </div>

</div>