***********************************************************
Blue Citron and Blue Citron Fluid themes
Author: Aaron Hawkins
http:///www.pixelclever.com
***********************************************************
I have endeavored to make these themes as comprehensive as possible, paying close attention to theming requirements of most common modules and organizing the css according to display areas. However there may arise details that need to be added. If you find something glaring that should be changed you can submit a feature request on the theme's issue queue. 

On the bottom of the theme is a small PixelClever logo that links to my site. 
You can remove this logo if you like by removing the contents of the div with the id of "credit" on line 142 of page.tpl.php.
If you want to be nice, leaving it would be a gesture of appreciation since I invested many hours on this theme, and it would help me to get some additional visitors to my site. You could put your logo beside it if you altered it to show that we both helped in the creation of the final theme.

I can be contacted for customizations of this theme (or for outright redesigns or custom themes). 
Hope it serves you well,
Aaron