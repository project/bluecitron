<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body class="main <?php print phptemplate_body_class($left, $right); ?>">
  <!--[if IE]><div class="IE"><![endif]-->

    <div id="wrapper">
    <div id="container" class="clear-block">
    <div id="pad">  

    <div class="topwrapper">
    
    <?php if ($logo || $site_title) {
          print '<a href="'. check_url($base_path) .'" title="'. $site_title .'">';
          if ($logo) {
            print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
          }
            print $site_html .'</a>' ;
			    } 
			?>
      
      <div class="topright">  
         
        <div class="search-etc">
          <?php if ($search_box): ?>
            <div class="searcher">
            <div class="searchposition">
            <?php print $search_box ?>    
            </div>
            </div>
            <div class="clear-right"></div>
          <?php endif; ?>
        </div>
        <!--/search-etc-->
        <div class="clear-right"></div>
         <?php if (isset($primary_links)) : ?>
           <div class="menuwrapper">
           <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
           <div class="clear-right"></div>
           </div>
        <?php endif; ?>
      
      </div>
          
      <!--/topright-->
    </div>
      <!--/top wrapper-->
     <div id="lowerwrap">
     <div class="clearer"></div>
       
			 <?php if ($left): ?>
          <div id="sidebar-left" class="sidebar">
          <?php print $left ?>    
          </div>
       <?php endif; ?>
              
       <div id="center">
          <div id="squeeze">
            
            <?php if ($title){ ?>
            	<h2><?php print $title;?></h2>
            <?php }?>
                  
						<?php if ($breadcrumb) { ?>
              <div class="breadcrumb-section">
              <?php print $breadcrumb; ?>
              </div>
            <?php }?>
            <!--/breadcrumb-section-->
                    
            <?php if ($tabs) {?>
              <div id="tabsection">  
                <div class="tabwrap-1">
                  <div class="clearer"></div>
                  <?php if ($tabs): print '<ul class="tabs local-primary">'. $tabs .'</ul>'; endif; ?>
                  <div class="clearer"></div>
                </div>
                <!--/tabwrap-1-->
                <div class="tabwrap-2"> 
                   <?php 
                     if ($tabs2) { 
                     print '<ul class="tabs local-secondary">'. $tabs2 .'</ul>';
                     } 
                     else { print '<div class="spacer"></div>';}  
                    ?>
                      
                   <div class="clearer"></div>
                </div>
                <!--/tabwrap-2-->
              </div>
              <!--/tabsection-->
            <?php }?>
                  
                           
            <?php if ($help): print $help; endif; ?>
            <?php if ($messages): print $messages; endif; ?>
                
            <?php if ($header) { ?> <div id="header-region" class="clear-block"><?php print $header; ?></div><?php } ?>
                  
            <?php print $content ?>
            
           <?php if ($feed_icons) { ?> <div class="feed"><?php print $feed_icons ?></div>  <?php }?>
            <div class="clearer"></div>
            
              
         </div>
       </div>
       <!-- /#squeeze, /#center -->
        
       <?php if ($right): ?>
          <div id="sidebar-right" class="sidebar">
          <?php print $right ?>
          </div>
       <?php endif; ?>
       <div class="clearer"></div>
          
       <div id="footer">
         <div class="clearer"></div>
				 <?php print $footer ?>
         <div class="clearer"></div>
       </div>
         
       <div class="clearer"></div>
       
     </div>
     <!-- /#lowerwrap -->
        
   </div>
   <!--/#pad-->
   
   <div class="sub">
   
     <div id="credit">
      
      <?php 
			 global $base_url;
			 $options = array (
			   'attributes' => array(
				   'title' => t('Drupal 6 Theme by PixelClever.com'),
				 ),
				  'html' => TRUE,				 
			 );
			 print l('<img src ="'.$base_url . '/' . path_to_theme() . '/images/pixelclever-mini-logo.png" alt="pixelclever" />', 'http://www.pixelclever.com', $options); 
			 ?>
     
     </div>
     <div class="clearer"></div>
   </div>
      
   </div> 
   <!-- /container -->
   
   </div>
   <!-- /wrapper -->

  <?php print $closure ?>
  <!--[if IE]></div><![endif]-->
  </body>
</html>
